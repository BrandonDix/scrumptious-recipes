from django.apps import AppConfig

from scrumptious.settings import DEFAULT_AUTO_FIELD


class RecipesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "recipes"


class ShoppingItemConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ShoppingItem"
